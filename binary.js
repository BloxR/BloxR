var fs = require('fs');
var world = require('./world.json');

var blockBits = 16;
// BBBB BBBB  BBBB SSSS
var worldBytes = 12;

// console.log(blockBits, world.height, world.width);
if (!ArrayBuffer.transfer) {
    ArrayBuffer.transfer = function(source, length) {
        if (!(source instanceof ArrayBuffer))
            throw new TypeError('Source must be an instance of ArrayBuffer');
        if (length <= source.byteLength)
            return source.slice(0, length);
        var sourceView = new Uint8Array(source),
            destView = new Uint8Array(new ArrayBuffer(length));
        destView.set(sourceView);
        return destView.buffer;
    };
}

// var binary = new ArrayBuffer(worldBytes + (world.height*world.width*16*16*(blockBits/8))); //worldBytes + (world.height*world.width*16*16*(blockBits/16))


function setB(v,p,s){
    for(var i = 0;i < Math.ceil(s.length/8);i++){
        while(binary.byteLength<=p+i){
            console.log(binary.byteLength);
            binary = ArrayBuffer.transfer(binary,binary.byteLength+100);
            view = new DataView(binary);
        }
        let q = s.slice(8*i,8*(i+1));
        while(q.length<8)q+='0';
        view.setInt8(p+i,parseInt(q,2));
    }
}
function bs(n,l){
    let s=n.toString(2);
    while(s.length<l)s='0'+s;
    return s;
}

var c = 0;

var data = "";
data += bs(world.height,12)+bs(world.width,12);
c+=3;
for(var x = 0;x < world.width;x++){
    for(var y = 0;y < world.height;y++){
        let reg = world.regions[`${x}-${y}`];
        for(var i = 0;i < 16;i++){
            for(var j = 0;j < 16;j++){
                if(reg[i][j]<128 && !reg[i][j].state){
                    data += bs(reg[i][j],8);
                    c+=1;
                } else if(!reg[i][j].state){
                    data += '10'+bs(reg[i][j],14);
                    c+=2;
                } else if(reg[i][j].state){
                    data += '11'+bs(reg[i][j].val,10)+bs(reg[i][j].state,4);
                    c+=2;
                }
            }
        }
    }
}

var binary = new ArrayBuffer(c);
// var binary = new ArrayBuffer(worldBytes + (world.height*world.width*16*16*(blockBits/8))); //worldBytes + (world.height*world.width*16*16*(blockBits/16))
var view = new DataView(binary);
setB(view,0,data);

fs.writeFileSync('binary',view);