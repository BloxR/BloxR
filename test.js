var fs = require('fs');
var world = require('./world.json');
// if(fs.existsSync('binary')){
//     var b = fs.readFileSync('binary');
//     var binary = b.buffer.slice(b.byteOffset, b.byteOffset + b.byteLength);
// } else {
    var binary = new ArrayBuffer(3);
// }

function setB(v,p,s){
    for(var i = 0;i < Math.ceil(s.length/8);i++){
        let q = s.slice(8*i,8*(i+1));
        while(q.length<8)q+='0';
        view.setInt8(p+i,parseInt(q,2));
    }
    // view.setInt8(p,parseInt(s,2));
}
function bs(n,l){
    let s=n.toString(2);
    while(s.length<l)s='0'+s;
    return s;
}

var view = new DataView(binary);

setB(view,0,bs(world.height,12)+bs(world.width,12));

// setB(view,0,'110011001100');

fs.writeFileSync('binary',view);