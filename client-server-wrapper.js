function runClientServer(){
    var world = null;
    if(localStorage.w1) {
        world = JSON.parse(localStorage.w1);
    }
    var {tick, world, receive, sendFunc} = x(world);
    
    var loop = setInterval(tick,1000/60);
    var username;
    
    sendFunc(function send(name,message){
        cReceive(message);
    });
    var cReceive = function(){};
    
    function incoming(message) {
        // message=JSON.parse(message);
        if(message.message == "connect"){
            username = message.username;
        }
        receive(username,message);
    }
    
    function stop(){
        console.log('Stopping Server.');
        clearInterval(loop);
        console.log('Saving World.');
        localStorage.world = JSON.stringify(world.toJSON());
        console.log('Saved World.');
        console.log('Exiting.');
        return true;
    }
    return {
        stop: stop,
        send: incoming,
        recFunc: function(rec){cReceive = rec}
    };
}