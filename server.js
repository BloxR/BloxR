function x(worldJSON){function Alea(){return(function(args){var s0=0;var s1=0;var s2=0;var c=1;if(args.length==0){args=[+new Date]}var mash=Mash();s0=mash(' ');s1=mash(' ');s2=mash(' ');for(var i=0;i<args.length;i+=1){s0-=mash(args[i]);if(s0<0){s0+=1}s1-=mash(args[i]);if(s1<0){s1+=1}s2-=mash(args[i]);if(s2<0){s2+=1}}mash=null;var random=function(){var t=2091639*s0+c*2.3283064365386963e-10;s0=s1;s1=s2;return s2=t-(c=t|0)};random.uint32=function(){return random()*0x100000000};random.fract53=function(){return random()+(random()*0x200000|0)*1.1102230246251565e-16};random.version='Alea 0.9';random.args=args;return random}(Array.prototype.slice.call(arguments)))};function Mash(){var n=0xefc8249d;var mash=function(data){data=data.toString();for(var i=0;i<data.length;i+=1){n+=data.charCodeAt(i);var h=0.02519603282416938*n;n=h>>>0;h-=n;h*=n;n=h>>>0;h-=n;n+=h*0x100000000}return(n>>>0)*2.3283064365386963e-10};mash.version='Mash 0.9';return mash}var SimplexNoise=function(r){if(r==undefined){r=Math}this.grad3=[[1,1,0],[-1,1,0],[1,-1,0],[-1,-1,0],[1,0,1],[-1,0,1],[1,0,-1],[-1,0,-1],[0,1,1],[0,-1,1],[0,1,-1],[0,-1,-1]];this.p=[];for(var i=0;i<256;i+=1){this.p[i]=Math.floor(r.random()*256)}this.perm=[];for(var i=0;i<512;i+=1){this.perm[i]=this.p[i&255]}this.simplex=[[0,1,2,3],[0,1,3,2],[0,0,0,0],[0,2,3,1],[0,0,0,0],[0,0,0,0],[0,0,0,0],[1,2,3,0],[0,2,1,3],[0,0,0,0],[0,3,1,2],[0,3,2,1],[0,0,0,0],[0,0,0,0],[0,0,0,0],[1,3,2,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[1,2,0,3],[0,0,0,0],[1,3,0,2],[0,0,0,0],[0,0,0,0],[0,0,0,0],[2,3,0,1],[2,3,1,0],[1,0,2,3],[1,0,3,2],[0,0,0,0],[0,0,0,0],[0,0,0,0],[2,0,3,1],[0,0,0,0],[2,1,3,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[2,0,1,3],[0,0,0,0],[0,0,0,0],[0,0,0,0],[3,0,1,2],[3,0,2,1],[0,0,0,0],[3,1,2,0],[2,1,0,3],[0,0,0,0],[0,0,0,0],[0,0,0,0],[3,1,0,2],[0,0,0,0],[3,2,0,1],[3,2,1,0]]};SimplexNoise.prototype.dot=function(g,x,y){return g[0]*x+g[1]*y};SimplexNoise.prototype.noise=function(xin,yin){var n0,n1,n2;var F2=0.5*(Math.sqrt(3.0)-1.0);var s=(xin+yin)*F2;var i=Math.floor(xin+s);var j=Math.floor(yin+s);var G2=(3.0-Math.sqrt(3.0))/6.0;var t=(i+j)*G2;var X0=i-t;var Y0=j-t;var x0=xin-X0;var y0=yin-Y0;var i1,j1;if(x0>y0){i1=1;j1=0}else{i1=0;j1=1}var x1=x0-i1+G2;var y1=y0-j1+G2;var x2=x0-1.0+2.0*G2;var y2=y0-1.0+2.0*G2;var ii=i&255;var jj=j&255;var gi0=this.perm[ii+this.perm[jj]]%12;var gi1=this.perm[ii+i1+this.perm[jj+j1]]%12;var gi2=this.perm[ii+1+this.perm[jj+1]]%12;var t0=0.5-x0*x0-y0*y0;if(t0<0){n0=0.0}else{t0*=t0;n0=t0*t0*this.dot(this.grad3[gi0],x0,y0)}var t1=0.5-x1*x1-y1*y1;if(t1<0){n1=0.0}else{t1*=t1;n1=t1*t1*this.dot(this.grad3[gi1],x1,y1)}var t2=0.5-x2*x2-y2*y2;if(t2<0){n2=0.0}else{t2*=t2;n2=t2*t2*this.dot(this.grad3[gi2],x2,y2)}return 70.0*(n0+n1+n2)};SimplexNoise.prototype.noise3d=function(xin,yin,zin){var n0,n1,n2,n3;var F3=1.0/3.0;var s=(xin+yin+zin)*F3;var i=Math.floor(xin+s);var j=Math.floor(yin+s);var k=Math.floor(zin+s);var G3=1.0/6.0;var t=(i+j+k)*G3;var X0=i-t;var Y0=j-t;var Z0=k-t;var x0=xin-X0;var y0=yin-Y0;var z0=zin-Z0;var i1,j1,k1;var i2,j2,k2;if(x0>=y0){if(y0>=z0){i1=1;j1=0;k1=0;i2=1;j2=1;k2=0}else if(x0>=z0){i1=1;j1=0;k1=0;i2=1;j2=0;k2=1}else{i1=0;j1=0;k1=1;i2=1;j2=0;k2=1}}else{if(y0<z0){i1=0;j1=0;k1=1;i2=0;j2=1;k2=1}else if(x0<z0){i1=0;j1=1;k1=0;i2=0;j2=1;k2=1}else{i1=0;j1=1;k1=0;i2=1;j2=1;k2=0}}var x1=x0-i1+G3;var y1=y0-j1+G3;var z1=z0-k1+G3;var x2=x0-i2+2.0*G3;var y2=y0-j2+2.0*G3;var z2=z0-k2+2.0*G3;var x3=x0-1.0+3.0*G3;var y3=y0-1.0+3.0*G3;var z3=z0-1.0+3.0*G3;var ii=i&255;var jj=j&255;var kk=k&255;var gi0=this.perm[ii+this.perm[jj+this.perm[kk]]]%12;var gi1=this.perm[ii+i1+this.perm[jj+j1+this.perm[kk+k1]]]%12;var gi2=this.perm[ii+i2+this.perm[jj+j2+this.perm[kk+k2]]]%12;var gi3=this.perm[ii+1+this.perm[jj+1+this.perm[kk+1]]]%12;var t0=0.6-x0*x0-y0*y0-z0*z0;if(t0<0){n0=0.0}else{t0*=t0;n0=t0*t0*this.dot(this.grad3[gi0],x0,y0,z0)}var t1=0.6-x1*x1-y1*y1-z1*z1;if(t1<0){n1=0.0}else{t1*=t1;n1=t1*t1*this.dot(this.grad3[gi1],x1,y1,z1)}var t2=0.6-x2*x2-y2*y2-z2*z2;if(t2<0){n2=0.0}else{t2*=t2;n2=t2*t2*this.dot(this.grad3[gi2],x2,y2,z2)}var t3=0.6-x3*x3-y3*y3-z3*z3;if(t3<0){n3=0.0}else{t3*=t3;n3=t3*t3*this.dot(this.grad3[gi3],x3,y3,z3)}return 32.0*(n0+n1+n2+n3)};
var noises = {};

var crossEq=function(points,triangle){var pa=points.a;var pb=points.b;var pc=points.c;var p0=triangle.a;var p1=triangle.b;var p2=triangle.c;var dXa=pa.x-p2.x;var dYa=pa.y-p2.y;var dXb=pb.x-p2.x;var dYb=pb.y-p2.y;var dXc=pc.x-p2.x;var dYc=pc.y-p2.y;var dX21=p2.x-p1.x;var dY12=p1.y-p2.y;var D=dY12*(p0.x-p2.x)+dX21*(p0.y-p2.y);var sa=dY12*dXa+dX21*dYa;var sb=dY12*dXb+dX21*dYb;var sc=dY12*dXc+dX21*dYc;var ta=(p2.y-p0.y)*dXa+(p0.x-p2.x)*dYa;var tb=(p2.y-p0.y)*dXb+(p0.x-p2.x)*dYb;var tc=(p2.y-p0.y)*dXc+(p0.x-p2.x)*dYc;if(D<0){return((sa<=0&&sb<=0&&sc<=0)||(ta<=0&&tb<=0&&tc<=0)||(sa+ta>=D&&sb+tb>=D&&sc+tc>=D))}return((sa>=0&&sb>=0&&sc>=0)||(ta>=0&&tb>=0&&tc>=0)||(sa+ta<=D&&sb+tb<=D&&sc+tc<=D))};var cross=function(points,triangle){var pa=points.a;var pb=points.b;var pc=points.c;var p0=triangle.a;var p1=triangle.b;var p2=triangle.c;var dXa=pa.x-p2.x;var dYa=pa.y-p2.y;var dXb=pb.x-p2.x;var dYb=pb.y-p2.y;var dXc=pc.x-p2.x;var dYc=pc.y-p2.y;var dX21=p2.x-p1.x;var dY12=p1.y-p2.y;var D=dY12*(p0.x-p2.x)+dX21*(p0.y-p2.y);var sa=dY12*dXa+dX21*dYa;var sb=dY12*dXb+dX21*dYb;var sc=dY12*dXc+dX21*dYc;var ta=(p2.y-p0.y)*dXa+(p0.x-p2.x)*dYa;var tb=(p2.y-p0.y)*dXb+(p0.x-p2.x)*dYb;var tc=(p2.y-p0.y)*dXc+(p0.x-p2.x)*dYc;if(D<0){return((sa>=0&&sb>=0&&sc>=0)||(ta>=0&&tb>=0&&tc>=0)||(sa+ta<=D&&sb+tb<=D&&sc+tc<=D))}return((sa<=0&&sb<=0&&sc<=0)||(ta<=0&&tb<=0&&tc<=0)||(sa+ta>=D&&sb+tb>=D&&sc+tc>=D))};var tEq=function(t0,t1){return!(crossEq(t0,t1)||crossEq(t1,t0))};var tOv=function(t0,t1){return!(cross(t0,t1)||cross(t1,t0))};

function rnd(s, a, x, y, z){
    if(!noises[s]) noises[s] = new SimplexNoise({random:new Alea(s)});
    return (noises[s].noise((x||0)*a,(y||0)*a,(z||0)*a) * 0.5) + 0.5
}

function sh(x1,y1,x2,y2,action,noHit){
    return [
        new hitbox({
            a:{x:x1/16,y:y1/16},
            b:{x:x2/16,y:y2/16},
            c:{x:x1/16,y:y2/16}
        }),
        new hitbox({
            a:{x:x1/16,y:y1/16},
            b:{x:x2/16,y:y2/16},
            c:{x:x2/16,y:y1/16}
        })
    ];
}

class hitbox {
    constructor(t, action, noHit){
        // if(x1!==undefined && x2!==undefined && y1!==undefined && x2!==undefined){
            this.t = t;
            // this.x1 = x1/16;
            // this.x2 = x2/16;
            // this.y1 = y1/16;
            // this.y2 = y2/16;
            this.noHit = noHit || {};
            this.action = action || null;
            // this.check = function(x,y,i,j,e,st){
                // let a = {
                //     top: Math.ceil((e.y + e.height/16 + j)*1600)/1600,
                //     bottom: Math.floor((e.y + j)*1600)/1600,
                //     left: Math.ceil((e.x-(e.width/32) + i)*1600)/1600,
                //     right: Math.floor((e.x+(e.width/32) + i)*1600)/1600
                // }
                // let b = {
                //     top: y + Math.max(this.y1,this.y2),
                //     bottom: y + Math.min(this.y1,this.y2),
                //     left: x + Math.min(this.x1,this.x2),
                //     right: x + Math.max(this.x1,this.x2),
                // }
                // if(st){
                //     return (
                //         a.bottom < b.top &&
                //         a.top > b.bottom &&
                //         a.left < b.right &&
                //         a.right > b.left
                //     )
                // }
                // let selfCheck = !st ? this.check(x,y,0,0,e,true) : st;
                // if(
                //     selfCheck ||
                //     (
                //         (
                //             (this.noHit.b && j>=0) || 
                //             (this.noHit.t && j<=0)
                //         ) && (
                //             (this.noHit.l && i>=0) || 
                //             (this.noHit.r && i<=0)
                //         )
                //     )
                // ){
                //     return {
                //         action:this.action?(
                //             (
                //                 a.bottom < b.top &&
                //                 a.top > b.bottom &&
                //                 a.left < b.right &&
                //                 a.right > b.left
                //             )?this.action:false
                //         ):false
                //     };
                // }
                // if(
                //     a.bottom >= b.top ||
                //     a.top <= b.bottom ||
                //     a.left >= b.right ||
                //     a.right <= b.left
                // ) {
                //     return {
                //         action:this.action?(
                //             (
                //                 a.bottom <= b.top &&
                //                 a.top >= b.bottom &&
                //                 a.left <= b.right &&
                //                 a.right >= b.left
                //             )?this.action:false
                //         ):false  
                //     };
                // }
            // }
        // } else if(t){
        //     this.check = t;
        // }
    }

    check(x2,y2,x,y,t){
        let a,b;
        let fix = function(l){
            return {x:l.x+x2,y:l.y+y2};
        }
        a = Object.assign({},this.t);
        a.a=fix(a.a);a.b=fix(a.b);a.c=fix(a.c);

        fix = function(l){
            return {x:l.x+x,y:l.y+y};
        }
        b = Object.assign({},t);
        b.a=fix(b.a);b.b=fix(b.b);b.c=fix(b.c);

        let eq = tEq(a,b);

        // console.log(a,b);

        if(tOv(a,b)){
            // console.log(a,b,x,y,x2,y2,t,this.t);


            /*
            a   { a: 
                    { x: 0, y: -0.001 },
                    b: { x: 1, y: 0.999 },
                    c: { x: 0, y: 0.999 } 
                }
            b   { a: { x: 0, y: 0 }, b: { x: 1, y: 1 }, c: { x: 1, y: 0 } } 
            x   0 
            y   0 
            x2  0 
            y2 -0.001 
            this.t  { a: { x: 0, y: 0 }, b: { x: 1, y: 1 }, c: { x: 1, y: 0 } } 
            t       { a: { x: 0, y: 0 }, b: { x: 1, y: 1 }, c: { x: 0, y: 1 } }
            */
            return false;
        } else {
            return {};
        }
    }
}

class block {
    constructor(id, textures, hitboxes){
        this.id = id;
        this.textures = textures;
        if(hitboxes){
            this.hitboxes = hitboxes;
        } else {
            this.hitboxes = [...sh(0,0,16,16)];
        }
    }
}

class tile {
    constructor(type,data){
        this.t = blockkeys.indexOf(type);
        if(data) this.d = data;
    }

    toJSON(){
        if(this.d){
            return {t:this.t,d:this.d};
        } else {
            return this.t;
        }
    }

    static fromJSON(json,blockz){
        if(json.t){
            return new tile(blockz[json.t],json.d);
        } else {
            return new tile(blockz[json]);
        }
    }
}

class region {
    constructor(x,y,h,w) {
        if(x===undefined)return;
        this.tiles = [];
        for(var i = 0;i < 16;i++){
            this.tiles[i] = [];
            for(var j = 0;j < 16;j++){
                let bloc = 'blox:air';
                if(x < 0 || x >= w || y < 0 || y >= h){
                    bloc = 'blox:barrier'
                }
                this.tiles[i][j] = new tile(bloc);
            }
        }
    }

    toJSON(){
        var d = this.tiles.map(i=>i.map(j=>{
            return j.toJSON();
        }));
        return d;
    }

    static fromJSON(json,blockz){
        json = json.map(i=>i.map(j=>{
            return tile.fromJSON(j,blockz);
        }))
        return Object.assign(new region(), {tiles: json});
    }
}

// class region {
//     constructor(x,y,h,w,l,s){
//         this.layers = [
//             new layer(x,y,h,w,l,s), //Backdrop
//             new layer(x,y,h,w,l,s), //Furniture
//             new layer(x,y,h,w,l,s), //Main
//             new layer(x,y,h,w,l,s), //
//             new layer(x,y,h,w,l,s)
//         ]
//     }
// }

class invItem {
    constructor(item,amount){
        this.amount = amount || (item?1:0);
        this.item = item || null;
    }

    toJSON(){
        var d = {
            item: this.item,
            amount: this.amount
        };
        return d;
    }

    static fromJSON(json){
        return Object.assign(new invItem(json.item,json.amount), json);
    }
}

class item {
    constructor(id, maxStack, type, attackDamage, block){
        this.id = id;
        this.maxStack = maxStack;
        this.type = type || 'default';
        this.attackDamage = attackDamage || 3;
        this.block = block || null;
    }
}

class inventory {
    constructor(size,preset){
        this.items = [];
        if(!size)size=32;
        if(!preset)preset=[];
        for(var i = 0;i < size;i++){
            this.items[i] = preset[i] || new invItem();
        }
        this.size = size;
    }

    toJSON(){
        var d = {
            items: this.items.map(i=>i.toJSON()),
            size: this.size
        };
        return d;
    }

    static fromJSON(json){
        json.items = json.items.map(i=>invItem.fromJSON(i));
        return Object.assign(new inventory(json.size), json);
    }
}

class entityType {
    constructor(id,hitboxes,maxHealth,defaultState,tick,inventory){
        this.id = id;
        this.hitboxes = hitboxes;
        this.weight
        this.maxHealth = maxHealth || 10;
        this.inventory = inventory;
        this.defaultState = defaultState || null;
        this.tick = tick || function(t){return t};
    }

    run(t) {
        t.veloy-=0.01;
        if(t.waterTime > 0 && t.veloy < 0.05){
            t.veloy+=0.005;
        }
        if(t.veloy==0)t.veloy=-0.01
        if(Math.abs(t.velox) >= 0.02){
            if(t.velox>=0.02){
                t.velox -= 0.012;
            } else if(t.velox<=0.02) {
                t.velox += 0.012;
            }
        } else {
            t.velox = 0;
        }
        if(t.velox > 0.9){
            t.velox = 0.9;
        } else if(t.velox < -0.9){
            t.velox = -0.9;
        }
        if(t.veloy > 0.9){
            t.veloy = 0.9;
        } else if(t.veloy < -0.9){
            t.veloy = -0.9;
        }
        let hiit = false;
        let max = {x:0,y:0,n:-1,actions:[]};
        var maxx = Math.abs(Math.floor(t.velox / 0.001));
        var maxy = Math.abs(Math.floor(t.veloy / 0.001));
        var incValX = 0.001;
        var incValY = 0.001;
        if(t.velox < 0) incValX*=-1;
        if(t.veloy < 0) incValY*=-1;
        for(var v = maxx+maxy;true;v--){
            for(var u = Math.max(0,v-maxy);u <= Math.min(v, maxx);u++){
                let i = u*incValX;
                let j = (v-u)*incValY;
                var possible = true;
                var actions = [];
                for(var k = 0;k < 12;k++){
                    let a = k%3 - 1;
                    let b = Math.floor(k/3)-1;
                    let bl = blocks[blockkeys[blockAt(t.x+i+a,t.y+j+b).t]];
                    for(var l = 0;l < bl.hitboxes.length;l++){
                        let hit = bl.hitboxes[l];
                        hiit = true;
                        for(var q = 0;q < t.hitboxes.length;q++){
                            let r = hit.check(Math.floor(t.x+i+a),Math.floor(t.y+j+b),t.x+i,t.y+j,t.hitboxes[q].t);
                            // let r = hit.check(Math.floor(t.x+i+a),Math.floor(t.y+j+b),i,j,t);
                            if(!r){
                                possible = false;
                                k = 12;
                                l = Infinity;
                                break;
                            }
                            if(r&&r.action) actions.push(r.action);
                        }
                    }
                }
                if(possible){
                    hiit = true;
                    max = {
                        x: i,
                        y: j,
                        n: Math.abs(i)+Math.abs(j),
                        actions: actions
                    };
                    v = 0;
                    break;
                }
            }
            if(v==0) break;
        }
        if(!hiit){
            console.log('NoHiit');
            max.x = t.velox;
            max.y = t.veloy;
        }
        if(t.waterTime>0){
            max.x = max.x/1.5;
            if(t.veloy < 0) max.y = max.y/3;
            max.y = Math.max(-0.1,max.y);
        }
        if(max.x > 0) t.facing = "right";
        if(max.x < 0) t.facing = "left";
        if(max.x == 0) {
            t.moving = false;
        } else {
            t.moving = true;
        }
        if(t.veloy < 0 && max.y == 0){
            t.onGround = true;
        } else {
            t.onGround = false;
        }
        t.x = Math.round((t.x + max.x)*10000)/10000;
        t.y = Math.round((t.y + max.y)*10000)/10000;

        if(t.waterTime>0){
            max.x = max.x*1.5;
            if(t.veloy < 0) max.y = max.y*3;
        }
        t.velox = max.x;
        t.veloy = max.y;
        for(var i = 0;i < max.actions.length;i++){
            t=max.actions[i](t);
        }
        t = this.tick(t);
        return t;
    }
}

var skins = {
    'blox:moblox': {
        height: 28,
        width: 14
    },'blox:f1': {
        height: 29,
        width: 14
    },'blox:f2': {
        height: 29,
        width: 14
    },'blox:m1': {
        height: 30,
        width: 14
    },'blox:m3': {
        height: 28,
        width: 14
    }
}
var entities = {
    'blox:slime_blue':new entityType(
        'blox:slime_blue', // ID
        [], // Hitboxes
        10, // Max Health
        null, // Default State
        function(e){ // Tick Function
            e.pressing["jump"]=true; //Is always Jumping
            return e;
        },
        null // Inventory
    ),
    'blox:player':new entityType(
        'blox:player', // ID
        [...sh(-7,0,7,30)], // Hitboxes
        10, // Max Health
        Object.keys(skins)[0], // Default State
        null, // Tick Function
        new inventory(32, [new invItem('blox:cactus')]) // Inventory
    )
}

class entity {
    constructor(x,y,type){
        this.id = Math.floor(Math.random()*10000);
        this.type = type;
        this.inventory = new inventory(entities[this.type].inventory.size,entities[this.type].items);
        this.health = entities[this.type].maxHealth;
        this.x = x;
        this.y = y;
        this.state = entities[this.type].defaultState;
        this.velox = 0;
        this.veloy = 0;
        this.hitboxes = entities[this.type].hitboxes;
        this.facing = "right";
        this.onGround = false;
        this.pressing = {};
        this.moving = false;
        this.waterTime = 0;
        this.air = 10000;
    }

    toJSON(){
        var d = {
            id: this.id,
            inventory: this.inventory?this.inventory.toJSON():null,
            health: this.health,
            x: this.x,
            y: this.y,
            state: this.state,
            velox: this.velox,
            veloy: this.veloy,
            onGround: this.onGround,
            moving: this.moving,
            air: this.air,
            type: this.type
        };
        return d;
    }

    static fromJSON(json){
        if(json.inventory) json.inventory = inventory.fromJSON(json.inventory);
        json.online = false;
        return Object.assign(new entity(json.x, json.y,json.type), json);
    }
}

class player extends entity {
    constructor(name,x,y){
        super(x,y,'blox:player');
        // this.id = Math.floor(Math.random()*10000);
        this.name = name;
        // this.inventory = new inventory(32, [new invItem('blox:cactus')]);
        // this.health = 10;
        // this.x = x;
        // this.y = y;
        // this.skin = Object.keys(skins)[0];
        // this.facing = "right";
        // this.velox = 0;
        // this.veloy = 0;
        // this.height = 30; // Remove Entity Properties. Sort out Facing, calcVelo.
        // this.width = 14;
        this.deathMessage = "died";
        // this.onGround = false;
        this.online = true;
        // this.pressing = {};
        // this.moving = false;
        this.selectedSlot = 0;
        // this.waterTime = 0;
        // this.air = 10000;
    }

    toJSON(){
        var d = {
            id: this.id,
            name: this.name,
            inventory: this.inventory.toJSON(),
            health: this.health,
            x: this.x,
            y: this.y,
            hitboxes: this.hitboxes,
            state: this.state,
            velox: this.velox,
            veloy: this.veloy,
            deathMessage: this.deathMessage,
            onGround: this.onGround,
            online: this.online,
            moving: this.moving,
            selectedSlot: this.selectedSlot,
            air: this.air,
            facing: this.facing
        };
        return d;
    }

    static fromJSON(json){
        json.inventory = inventory.fromJSON(json.inventory);
        json.online = false;
        return Object.assign(new player(json.name, json.x, json.y), json);
    }

    nextSkin(){
        this.state = Object.keys(skins)[(Object.keys(skins).indexOf(this.state)+1) % Object.keys(skins).length];
    }
}

class worldOptions {
    constructor({name, difficulty}){
        this.difficulty = difficulty;
        this.name = name;
    }

    toJSON(){
        return {
            difficulty: this.difficulty,
            name: this.name
        }
    }

    static fromJSON(json){
        return Object.assign(new worldOptions({}), json);
    }
}

class chat {
    constructor(){
        this.messages = [];
    }
    newMessage(player,text){
        if(text.startsWith('/')){
            if(text.startsWith('/tp')){
                let args = text.split(' ');
                if(Number(args[1]) && Number(args[2])){
                    player.x = Number(args[1])+0.5;
                    player.y = Number(args[2])+0.5;
                    send(player.name,{message:"command_response",text:`Teleported to ${player.x} ${player.y}`});
                } else {
                    send(player.name,{message:"command_response",text:"Usage: /tp <x> <y>"});
                }
            } else if(text.startsWith('/help')){
                let response = "Command:\n/help - This command.\n/tp <x> <y> - Teleport yourself to the specified co-ordinates.\n/list - Lists all Players on the server";
                for(var i = 0;i < response.split('\n').length;i++){
                    send(player.name,{message:"command_response",text:response.split('\n')[i]});
                }
            } else if(text.startsWith('/list')){
                let response = "Players:\n"+w.players.filter(i=>i.online).map(i=>i.name).join('\n');
                for(var i = 0;i < response.split('\n').length;i++){
                    send(player.name,{message:"command_response",text:response.split('\n')[i]});
                }
            } else {
                send(player.name,{message:"command_response",text:"Invalid Command"});
            }
        } else {
            this.messages.push({player:player,text:text});
            send("all",{message:"chat",player:player,text:text});
        }
        return player;
    }
}

class world {
    constructor(height, width, options, seed) {
        var textures = {};
        Object.entries(blocks).forEach(i=>textures[i[0]] = i[1].textures);
        this.textures = textures;
        this.chat = new chat();
        if(height === "nogen")return;
        this.height = height;
        this.width = width;
        this.regions = {};
        this.options = options;
        this.level = height/2;
        this.entities = [];
        // this.players = [];
        this.spawnx = width*8;
        this.spawny = 13 + (this.level*16);
        this.players = [];
        this.blocks = Object.keys(blocks);
        // this.player = new player('MOBlox', this.spawnx, this.spawny);
        this.seed = Math.floor(seed || Math.random()*(65536*65536));
        // this.seeds = [Math.floor(this.seed / 65536), Math.floor(this.seed % 65536)];
        this.seeds = (this.seed).toString(2)
        while(this.seeds.length<32){
            this.seeds="0"+this.seeds;
        }
        
        for(var x = -1;x <= this.width;x++){
            for(var y = -1;y <= this.height;y++){
                this.regions[`${x}-${y}`] = new region(x,y,this.height,this.width,this.level,this.seeds);
            }
            console.log('Building World:',Math.round(x/this.width*100)+'%');
        }
        for(var x = 0;x < this.width*16;x++){
            let r =  rnd(parseInt(this.seeds.slice(0,8),2),  0.025, x - (this.width*8));
            let rb = rnd(parseInt(this.seeds.slice(8,12),2),  0.025, x - (this.width*8));
            let stoneHeight = Math.floor(r * 16) + (this.level*16);
            let dirtHeight = stoneHeight + Math.floor(rb * 5) + 2;
            let biomes = ['snow','ocean','grass','desert'];
            let biome = biomes[ Math.floor( (x / 16) / (this.width / biomes.length) ) ];
            for(var y = 0;y < this.height*16;y++){
                let bloc = 'blox:air';
                if(x < 0 || x >= this.width*16 || y < 0 || y >= this.height*16){
                    bloc = 'blox:barrier'
                } else {
                    if(y <= dirtHeight){
                        if(biome == 'desert'){
                            bloc = 'blox:sand';
                        } else if(biome == 'snow') {
                            bloc = 'blox:dirt_cold';
                        } else if(biome == 'ocean') {
                            bloc = 'blox:water';
                        } else {
                            bloc = 'blox:dirt';
                        }
                    }
                    if(y < stoneHeight){
                        if(biome == 'snow'){
                            bloc = 'blox:stone_cold';
                        } else if(biome == 'ocean') {
                            bloc = 'blox:water';
                        } else {
                            bloc = 'blox:stone';
                        }
                    }
                }
                setBlockAt(this,x,y,new tile(bloc));
            }
        }
        var caveMap = [];
        for(var x = 0;x < this.width*16;x++){
            caveMap[x] = [];
            for(var y = 0;y < this.height*16;y++){
                var r = rnd(parseInt(this.seeds.slice(16,20),2),0.05,x,y);
                let bloc=false;
                if(r>0.75) {
                    bloc=true;
                }
                caveMap[x][y] = bloc;
            }
        }
        let iterations = 20;
        for(var z = 0;z < iterations;z++){
            console.log('Mining Caves:',Math.round((z+1)/iterations*100)+'%');
            let newmap = [];
            for(var x = 0;x < this.width*16;x++){
                newmap[x] = [];
                for(var y = 0;y < this.height*16;y++){
                    let count = 0;
                    for(var i = 0;i < 9;i++){
                        if(
                            caveMap[x+Math.floor(i/3)-1] && 
                            caveMap[x+Math.floor(i/3)-1][y+(i%3)-1]
                        ){
                            count++;
                        }
                    }
                    if(caveMap[x][y]){
                        if(count < 4){
                            newmap[x][y] = false;
                        } else {
                            newmap[x][y] = true;
                        }
                    } else {
                        if(count > 5){
                            newmap[x][y] = true;
                        } else {
                            newmap[x][y] = false;
                        }
                    }
                }
            }
            caveMap = newmap;
        }
        for(var x = 0;x < this.width*16;x++){
            console.log('Growing Grass:',Math.round((x+1)/(this.width*16)*100)+'%');
            let air = true;
            for(var y = this.height*16-1;y >= 0;y--){
                if(caveMap[x][y]){
                    if(blockkeys[blockAt(x,y,this).t]!='blox:water'){
                        setBlockAt(this,x,y,new tile('blox:air'));
                    }
                }
                if(air==true&&blockkeys[blockAt(x,y,this).t]!='blox:air'){
                    air = false;
                    let b = blockkeys[blockAt(x,y,this).t];
                    if(b=='blox:dirt'){
                        b = 'blox:grass';
                    } else if(b=='blox:dirt_cold') {
                        b = 'blox:grass_snowy';
                    } else if(b=='blox:stone_cold'){
                        b = 'blox:stone_snowy';
                    }
                    setBlockAt(this,x,y,new tile(b));
                }
            }
        }
        for(var x = 0;x < this.width*16;x++){
            console.log('Growing Plants:',Math.round((x+1)/(this.width*16)*100)+'%');
            let air = true;
            for(var y = this.height*16-1;y >= 0;y--){
                if(air==true&&blockkeys[blockAt(x,y-1,this).t]!='blox:air'){
                    air=false;
                    let ub = blockkeys[blockAt(x,y-1,this).t];
                    let rd = rnd(parseInt(this.seeds.slice(12,16),2), 10, x - (this.width*8));
                    let bloc = 'blox:air';
                    if(ub == 'blox:sand'){
                        if(rd>0.8)bloc = 'blox:cactus'
                        if(rd>0.9)bloc = 'blox:cactus_flower'
                    } else if(ub == 'blox:grass_snowy') {
                        if(rd<0.1){
                            bloc = {t:'blox:structure_generator',d:{structure:'blox:tree_snowy'}};
                        }
                    } else if(ub == 'blox:grass') {
                        if(rd>0.75)bloc= 'blox:sunflower'
                        if(rd>0.8)bloc = 'blox:grass_plant'
                        if(rd>0.9)bloc = 'blox:grass_plant_flower'
                        if(rd<0.05){
                            bloc = {t:'blox:structure_generator',d:{structure:'blox:tree'}};
                        }
                    }
                    if(bloc.t){
                        setBlockAt(this,x,y,new tile(bloc.t,bloc.d));
                    } else {
                        setBlockAt(this,x,y,new tile(bloc));
                    }
                }
            }
        }
        for(var x = 0;x < this.width*16;x++){
            console.log('Building Structures:',Math.round((x+1)/(this.width*16)*100)+'%');
            for(var y = this.height*16-1;y >= 0;y--){
                if(blockkeys[blockAt(x,y,this).t]=='blox:structure_generator'){
                    // Multi-Block Structure Generation
                    let structure = blockAt(x,y,this).d.structure;
                    let r = rnd(parseInt(this.seeds.slice(20,24),2), 10, x - (this.width*8));
                    this.generateStructure(x,y,structure,r);
                }
            }
        }
    }

    join(name){
        if(this.players.map(i=>i.name).includes(name)){
            if(this.players[this.players.map(i=>i.name).indexOf(name)].online) return false;
            this.players[this.players.map(i=>i.name).indexOf(name)].online = true;
            return this.players[this.players.map(i=>i.name).indexOf(name)].id;
        } else {
            this.players.push(new player(name, this.spawnx, this.spawny));
            this.players[this.players.map(i=>i.name).indexOf(name)].online = true;
            return this.players[this.players.length-1].id;
        }
    }

    disconnect(name){
        this.players[this.players.map(i=>i.name).indexOf(name)].online = false;
    }

    toJSON(){
        var d = {
            height: this.height,
            width: this.width,
            regions: {},
            options: this.options.toJSON(),
            level: this.level,
            entities: this.entities.map(i=>i.toJSON()),
            spawnx: this.spawnx,
            spawny: this.spawny,
            players: this.players.map(i=>i.toJSON()),
            seed: this.seed,
            seeds: this.seeds,
            blocks: Object.keys(blocks),
            textures: this.textures
        }
        let rs = Object.keys(this.regions);
        for(var i = 0;i < rs.length;i++){
            let k = rs[i];
            let r = this.regions[k];
            d.regions[k] = r.toJSON();
        }
        return d;
    }

    static fromJSON(json){
        json.options = worldOptions.fromJSON(json.options);
        json.players = json.players.map(i=>player.fromJSON(i));
        json.entities = json.entities.map(i=>entity.fromJSON(i));
        var tmp = {};
        var regs = Object.keys(json.regions);
        for(var i = 0;i < regs.length;i++){
            let k = regs[i];
            tmp[k] = region.fromJSON(json.regions[k],json.blocks);
        }
        delete json.blocks;
        delete json.textures;
        json.regions = tmp;
        return Object.assign(new world("nogen"), json);
    }

    generateStructure(x,y,st,r){
        let s = structures[st][Math.floor(r*structures[st].length)];
        var blocks = s.blocks;
        var ov = !!s.overide;
        setBlockAt(this,x,y,new tile('blox:air'));
        for(var i = 0;i < s.tiles.length;i++){
            for(var j = 0;j < s.tiles[i].length;j++){
                let b = blocks[s.tiles[i][j]];
                if(blockkeys[blockAt(x+i+(s.offsetX||0),y+j+(s.offsetY||0),this).t]=='blox:air'){
                    setBlockAt(this,x+i+(s.offsetX||0),y+j+(s.offsetY||0),new tile(b));
                }
            }
        }
    }
}

// var w = new world(64,64,new worldOptions({name:'Banana',difficulty:1}));

function cactusHurt(e){if(currentTick%30==0){e.health-=0.7;e.deathMessage="was pricked to death"}return e;}
function inWater(e){e.waterTime = 2;return e}
var nh = {b:1,t:1,l:1,r:1};
blocks = {
    'blox:air': new block('blox:air',['blox:air'],[]),
    'blox:stone': new block('blox:stone',['blox:stone3']),
    'blox:stone_snowy': new block('blox:stone_snowy',['blox:stone_snowy']),
    'blox:grass': new block('blox:grass',['blox:grass4']),
    'blox:dirt': new block('blox:dirt',['blox:dirt']),
    'blox:dirt_cold': new block('blox:dirt_cold',['blox:dirt']),
    'blox:stone_cold': new block('blox:stone_cold',['blox:stone_cold']),
    'blox:wood_log': new block('blox:wood_log',['blox:wood_log']),
    'blox:wood_planks': new block('blox:wood_planks',['blox:wood_planks']),
    'blox:wood_wall': new block('blox:wood_wall',['blox:wood_wall'],[]),
    'blox:barrier': new block('blox:barrier',['blox:air']),
    'blox:leaves': new block('blox:leaves',['blox:leaves2'],[]),
    'blox:leaves_snowy': new block('blox:leaves_snowy',['blox:leaves_snowy'],[...sh(0,15,16,16,null,{b:1,l:1,r:1})]),
    'blox:grass_snowy': new block('blox:grass_snowy',['blox:grass_snowy']),
    'blox:sand': new block('blox:sand',['blox:sand3']),

    'blox:grass_plant': new block('blox:grass_plant',['blox:grass_plant'],[]),
    'blox:grass_plant_flower': new block('blox:grass_plant_flower',['blox:grass_plant_flower'],[]),
    'blox:sunflower': new block('blox:sunflower',['blox:sunflower'],[]),
    'blox:cactus': new block('blox:cactus',['blox:cactus'],[...sh(5,0,10,15,cactusHurt),...sh(0,6,5,14,cactusHurt),...sh(10,1,16,7,cactusHurt)]),
    'blox:cactus_flower': new block('blox:cactus_flower',['blox:cactus_flower'],[...sh(5,0,10,15,cactusHurt),...sh(0,6,5,14,cactusHurt),...sh(10,1,16,7,cactusHurt)]),
    'blox:structure_generator': new block('blox:structure_generator',['blox:missingno']),


    'blox:water': new block('blox:water',['blox:water4','blox:water6','blox:water7','blox:water8'],[...sh(0,0,16,16,inWater,nh)])
}
var structures = {
    'blox:tree':[
        {
            blocks:['blox:air','blox:wood_log','blox:leaves'],
            offsetX:-2,
            tiles:[
                [0,2,2,2,0],
                [0,2,2,2,2],
                [1,1,1,2,2],
                [0,2,2,2,2],
                [0,2,2,2,0]
            ]
        },
        {
            blocks:['blox:air','blox:wood_log','blox:leaves'],
            offsetX:-2,
            tiles:[
                [0,0,0,0,0,0,0,0,0,2,0,0],
                [0,0,0,0,0,0,0,0,2,2,2,0],
                [1,1,1,1,1,1,1,1,1,2,2,2],
                [0,0,0,1,0,0,0,0,2,2,2,2],
                [0,0,0,0,1,0,0,0,2,2,2,2],
                [0,0,0,0,0,1,0,0,2,2,2,2],
                [0,0,0,0,0,0,1,0,2,2,2,2],
                [0,0,0,0,0,0,0,1,1,2,2,0],
                [0,0,0,0,0,0,0,0,2,2,2,0],
                [0,0,0,0,0,0,0,0,2,0,0,0]
            ]
        }
        /*

        */
    ],'blox:tree_snowy':[
        {
            blocks:['blox:air','blox:wood_log','blox:leaves','blox:leaves_snowy'],
            offsetX:-2,
            tiles:[
                [0,2,2,3,0],
                [0,2,2,2,3],
                [1,1,1,2,3],
                [0,2,2,2,3],
                [0,2,2,3,0]
            ]
        },
        {
            blocks:['blox:air','blox:wood_log','blox:leaves','blox:leaves_snowy'],
            offsetX:-2,
            tiles:[
                [0,0,0,3,0,0],
                [1,0,0,2,3,0],
                [1,1,1,1,2,3],
                [1,1,1,1,2,3],
                [1,0,0,2,3,0],
                [0,0,0,3,0,0]
            ]
        }
    ]
}
/*
.###.
#####
##|##
..|..
*/
var blockkeys = Object.keys(blocks);
var items = {
    'blox:cactus': new item('blox:cactus', 64, 'block', 4, 'blox:cactus')
}
if(worldJSON){
    var w = world.fromJSON(worldJSON);
} else {
    var w = new world(16,64,new worldOptions({name:'Banana',difficulty:1}));
}

var currentTick = 0;
var tps = 0;
var lastZeroTickTime = Date.now();

var debug = false;

var send = function(){};
function sendFunc(sendF){
    send = sendF;
}

function receive(name, msg){
    switch(msg.message){
        case "connect":
            let id = w.join(name);
            send(name,{message:"textures",textures:Array.from(new Set(Object.values(blocks).map(i=>i.textures).flat()))});
            send(name,{message:"world",world:w.toJSON(),id:id});
            send("all",{message:"join",player:w.players.find(i=>i.id==id)})
            break;
        case "keys":
            w.players[w.players.findIndex(i=>i.name==name)].pressing = {
                left:!!msg.keys[0],
                right:!!msg.keys[1],
                jump:!!msg.keys[2]
            };
            break;
        case "disconnect":
            w.disconnect(name);
            send("all",{message:"leave",player:w.players.find(i=>i.name==name)})
            break;
        case "chat":
            w.players[w.players.findIndex(i=>i.name==name)] = w.chat.newMessage(w.players[w.players.findIndex(i=>i.name==name)],msg.text)
            break;
        case "switchSlot":
            w.players[w.players.findIndex(i=>i.name==name)].selectedSlot = msg.slot;
            break;
        case "nextSkin":
            w.players[w.players.findIndex(i=>i.name==name)].nextSkin();
            break;
        case "setBlock":
            if(msg.x < 0 || msg.y < 0 || msg.x >= w.width*16 || msg.y >= w.height*16){
                break;
            }
            setBlockAt(w,msg.x,msg.y,new tile(msg.block));
            send("all",{
                message: "region",
                region: `${Math.floor(msg.x/16)}-${Math.floor(msg.y/16)}`,
                data: w.regions[`${Math.floor(msg.x/16)}-${Math.floor(msg.y/16)}`].toJSON()
            });
            break;
        default:
            break;
    }
    // Switch msg.message, then do stuff with the msg.
}

function blockAt(ia,ja,wo){
    let i = Math.floor(ia/16);
    let j = Math.floor(ja/16);
    if(!wo)wo=w;
    if(!wo.regions[`${i}-${j}`])wo.regions[`${i}-${j}`]=new region(i,j,wo.height,wo.width,wo.level,wo.seeds);
    return wo.regions[`${i}-${j}`].tiles[Math.abs(Math.floor(ia))%16][Math.abs(Math.floor(ja))%16]
}
function setBlockAt(w,ia,ja,s){
    let i = Math.floor(ia/16);
    let j = Math.floor(ja/16);
    if(!w.regions[`${i}-${j}`])w.regions[`${i}-${j}`]=new region(i,j,w.height,w.width,w.level,w.seeds);
    return w.regions[`${i}-${j}`].tiles[Math.abs(Math.floor(ia))%16][Math.abs(Math.floor(ja))%16] = s;
}

function calcVelo(t){
    t.veloy-=0.01;
    if(t.waterTime > 0 && t.veloy < 0.05){
        t.veloy+=0.005;
    }
    if(t.veloy==0)t.veloy=-0.01
    if(Math.abs(t.velox) >= 0.02){
        if(t.velox>=0.02){
            t.velox -= 0.012;
        } else if(t.velox<=0.02) {
            t.velox += 0.012;
        }
    } else {
        t.velox = 0;
    }
    if(t.velox > 0.9){
        t.velox = 0.9;
    } else if(t.velox < -0.9){
        t.velox = -0.9;
    }
    if(t.veloy > 0.9){
        t.veloy = 0.9;
    } else if(t.veloy < -0.9){
        t.veloy = -0.9;
    }
    let hiit = false;

    //NEW PHYSICS
    let max = {x:0,y:0,n:-1,actions:[]};
    let incValX = 0.001;
    let incValY = 0.001;
    if(t.velox < 0) incValX*=-1;
    if(t.veloy < 0) incValY*=-1;
    for(var i = 0;true;i+=incValX){
        if(Math.abs(i) > Math.abs(t.velox)-0.002) i = t.velox;
        for(var j = t.veloy;true;j-=incValY){
            if(Math.abs(j) < 0.002) j = 0;
            if(Math.abs(i)+Math.abs(j) > max.n){
                var possible = true;
                var actions = [];
                for(var k = 0;k < 12;k++){
                    let a = k%3 - 1;
                    let b = Math.floor(k/3)-1;
                    let bl = blocks[blockkeys[blockAt(t.x+a,t.y+b).t]];
                    for(var l = 0;l < bl.hitboxes.length;l++){
                        let hit = bl.hitboxes[l];
                        hiit = true;
                        
                        let r = hit.check(Math.floor(t.x+a),Math.floor(t.y+b),i,j,t);
                        if(!r){
                            possible = false;
                            k = 12;
                            // break;
                        }
                        if(r&&r.action) actions.push(r.action);
                    }
                }
                if(possible){
                    hiit = true;
                    max = {
                        x: i,
                        y: j,
                        n: Math.abs(i)+Math.abs(j),
                        actions: actions
                    };
                    j = 0;
                }
            }
            if(j == 0) break;
        }
        if(i == t.velox) break;
    }
    // return {x:max.i,y:max.j,action: max.action!==false ? this.action : null};
    // NEW PHYSICS


    // for(var i = 0;i < 12;i++){
    //     let a = i%3 - 1;
    //     let b = Math.floor(i/3)-1;
    //     if(((a == -1 || a == 1) && !t.onGround) || ( b >= 0 && t.veloy >= 0 ) || ( b <= 1 && t.veloy <= 0 ) ){
    //         let bl = blocks[blockkeys[blockAt(t.x+a,t.y+b).t]];
    //         for(var j = 0;j < bl.hitboxes.length;j++){
    //             let hit = bl.hitboxes[j];
    //             hiit = true;
    //             let r = hit.check(Math.floor(t.x+a),Math.floor(t.y+b),t);
    //             if(r.action) actions.push(r.action);
    //             if(Math.abs(r.x) < Math.abs(minx)) minx = r.x;
    //             if(Math.abs(r.y) < Math.abs(miny)) miny = r.y;
    //         }
    //     }
    // }
    if(!hiit){
        console.log('NoHiit');
        max.x = t.velox;
        max.y = t.veloy;
    }
    if(t.waterTime>0){
        max.x = max.x/1.5;
        if(t.veloy < 0) max.y = max.y/3;
        max.y = Math.max(-0.1,max.y);
    }
    if(max.x > 0) t.facing = "right";
    if(max.x < 0) t.facing = "left";
    if(max.x == 0) {
        t.moving = false;
    } else {
        t.moving = true;
    }
    if(t.veloy == -0.01 && max.y == 0){
        t.onGround = true;
    } else {
        t.onGround = false;
    }
    t.x = Math.round((t.x + max.x)*16000)/16000;
    t.y = Math.round((t.y + max.y)*16000)/16000;

    if(t.waterTime>0){
        max.x = max.x*1.5;
        if(t.veloy < 0) max.y = max.y*3;
    }
    // if(max.x < t.velox){
        // t.velox=Math.max(max.x,t.velox-0.02);
    // } else if(max.x > t.velox){
        // t.velox=Math.min(max.x,t.velox+0.02);
    // }
    // if(max.y < t.veloy){
        // t.veloy=Math.max(max.y,t.veloy-0.02);
    // } else if(max.y > t.veloy){
        // t.veloy=Math.min(max.y,t.veloy+0.02);
    // }
    t.velox = max.x;
    t.veloy = max.y;
    for(var i = 0;i < max.actions.length;i++){
        t=max.actions[i](t);
    }
    return t;
}

function tick(){
    currentTick = currentTick+1;
    if(currentTick%60 == 0){
        tps = 60 / ((Date.now() - lastZeroTickTime) / 1000);
        lastZeroTickTime = Date.now();
        // console.log('Server TPS:',tps);
    }
    for(var i = 0;i < w.players.length;i++){
        if(w.players[i].online){
            let speed = 0.012;
            w.players[i].waterTime--;
            if(w.players[i].waterTime>0){
                w.players[i].air-=8;
            } else {
                w.players[i].air+=32;
            }
            w.players[i].air = Math.max(Math.min(w.players[i].air,10000),0);
            if(w.players[i].pressing["jump"] && (w.players[i].onGround)) {
                w.players[i].veloy += 0.254;
            } else if(w.players[i].pressing["jump"] && w.players[i].waterTime>0 && w.players[i].veloy < 0.1){
                w.players[i].veloy += 0.011;
            }
            if(w.players[i].pressing["left"]) {
                if(Math.abs(w.players[i].velox) <= 0.15) w.players[i].velox -= 0.007;
                if(Math.abs(w.players[i].velox) <= 0.01) w.players[i].velox -= 0.007;
                w.players[i].velox -= speed;
            }
            if(w.players[i].pressing["right"]) {
                if(Math.abs(w.players[i].velox) <= 0.15) w.players[i].velox += 0.007;
                if(Math.abs(w.players[i].velox) <= 0.01) w.players[i].velox += 0.007;
                w.players[i].velox += speed;
            }
            w.players[i] = entities[w.players[i].type].run(w.players[i]);
            // w.players[i] = calcVelo(w.players[i]);
            if(w.players[i].x < 0 || w.players[i].x > w.width*16 || w.players[i].y < 0 || w.players[i].y > w.height*16){
                w.players[i].health = 0;
                w.players[i].deathMessage = "fell out of the world";
            }
            if(w.players[i].health < 0.5){
                send("all", {message:"death",player:w.players[i],deathMessage:w.players[i].deathMessage});
                w.players[i].health = 10;
                w.players[i].x = w.spawnx;
                w.players[i].y = w.spawny;
                w.players[i].veloy = 0;
                w.players[i].velox = 0;
            }
        }
    }
    send("all", {message:"players",players:w.players.filter(i=>i.online)});
}
return {
    tick: tick,
    world: w,
    receive: receive,
    sendFunc: sendFunc
}
}
if(typeof module!=="undefined"){
    if (module.exports){
        module.exports = x
    }
} else {
    window.server = x
}