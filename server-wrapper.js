var fs = require('fs');
if(!fs.existsSync('./config.json')) fs.writeFileSync('config.json',JSON.stringify({
    world: "world"
},null,4));
var config = require('./config.json');
var world = null;
if(fs.existsSync(`./${config.world}.json`)) {
    world = JSON.parse(fs.readFileSync(`./${config.world}.json`,'utf8'));
    console.log('Loaded world from file.')
}
var {tick, world, receive, sendFunc} = require('./server.js')(world);

setInterval(tick,1000/60);

var express = require('express');
var app = express();
var expressWs = require('express-ws')(app);
function wss(){return expressWs.getWss()}
app.use(express.static('.'));
 
app.ws('/', function(ws, req) {
    ws.isAlive = true;
    ws.on('pong', heartbeat);
    ws.on('message', function incoming(message) {
        // console.log('received: %s', message);
        message=JSON.parse(message);
        if(message.message == "connect"){
            ws.username = message.username;
        }
        receive(ws.username,message);
    });
    ws.on('close', function(){
        receive(ws.username,{message:"disconnect"});
        console.log('Connection closed from ', ws.username);
    });
});

function noop() {}

function heartbeat() {
  this.isAlive = true;
}

sendFunc(function send(name,message){
    wss().clients.forEach(function each(ws) {
        if (ws.readyState === ws.OPEN) {
            if(message == "stop"){
                stop();
            }
            if(name=="all"||ws.username == name){
                ws.send(JSON.stringify(message));
            }
        }
    });
});
  
const interval = setInterval(function ping() {
    wss().clients.forEach(function each(ws) {
        if (ws.isAlive === false) return ws.terminate();

        ws.isAlive = false;
        ws.ping(noop);
    });
    save();
}, 30000);

process.on('SIGINT', stop);

function stop(){
    console.log('Stopping Server.');
    save();
    console.log('Exiting.');
    process.exit();
}
function save(){
    console.log('Saving World.');
    fs.writeFileSync(`./${config.world}.json`,JSON.stringify(world.toJSON()));
    console.log('Saved World.');
}

 
app.listen(3000);