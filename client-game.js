function resizeCanvasToDisplaySize(canvas) {
    width = canvas.clientWidth;
    height = canvas.clientHeight;
    if (canvas.width !== width || canvas.height !== height) {
        canvas.width = width;
        canvas.height = height;
        return true;
    }
 
    return false;
}
function Alea(){return(function(args){var s0=0;var s1=0;var s2=0;var c=1;if(args.length==0){args=[+new Date]}var mash=Mash();s0=mash(' ');s1=mash(' ');s2=mash(' ');for(var i=0;i<args.length;i+=1){s0-=mash(args[i]);if(s0<0){s0+=1}s1-=mash(args[i]);if(s1<0){s1+=1}s2-=mash(args[i]);if(s2<0){s2+=1}}mash=null;var random=function(){var t=2091639*s0+c*2.3283064365386963e-10;s0=s1;s1=s2;return s2=t-(c=t|0)};random.uint32=function(){return random()*0x100000000};random.fract53=function(){return random()+(random()*0x200000|0)*1.1102230246251565e-16};random.version='Alea 0.9';random.args=args;return random}(Array.prototype.slice.call(arguments)))};function Mash(){var n=0xefc8249d;var mash=function(data){data=data.toString();for(var i=0;i<data.length;i+=1){n+=data.charCodeAt(i);var h=0.02519603282416938*n;n=h>>>0;h-=n;h*=n;n=h>>>0;h-=n;n+=h*0x100000000}return(n>>>0)*2.3283064365386963e-10};mash.version='Mash 0.9';return mash}var SimplexNoise=function(r){if(r==undefined){r=Math}this.grad3=[[1,1,0],[-1,1,0],[1,-1,0],[-1,-1,0],[1,0,1],[-1,0,1],[1,0,-1],[-1,0,-1],[0,1,1],[0,-1,1],[0,1,-1],[0,-1,-1]];this.p=[];for(var i=0;i<256;i+=1){this.p[i]=Math.floor(r.random()*256)}this.perm=[];for(var i=0;i<512;i+=1){this.perm[i]=this.p[i&255]}this.simplex=[[0,1,2,3],[0,1,3,2],[0,0,0,0],[0,2,3,1],[0,0,0,0],[0,0,0,0],[0,0,0,0],[1,2,3,0],[0,2,1,3],[0,0,0,0],[0,3,1,2],[0,3,2,1],[0,0,0,0],[0,0,0,0],[0,0,0,0],[1,3,2,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[1,2,0,3],[0,0,0,0],[1,3,0,2],[0,0,0,0],[0,0,0,0],[0,0,0,0],[2,3,0,1],[2,3,1,0],[1,0,2,3],[1,0,3,2],[0,0,0,0],[0,0,0,0],[0,0,0,0],[2,0,3,1],[0,0,0,0],[2,1,3,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0],[2,0,1,3],[0,0,0,0],[0,0,0,0],[0,0,0,0],[3,0,1,2],[3,0,2,1],[0,0,0,0],[3,1,2,0],[2,1,0,3],[0,0,0,0],[0,0,0,0],[0,0,0,0],[3,1,0,2],[0,0,0,0],[3,2,0,1],[3,2,1,0]]};SimplexNoise.prototype.dot=function(g,x,y){return g[0]*x+g[1]*y};SimplexNoise.prototype.noise=function(xin,yin){var n0,n1,n2;var F2=0.5*(Math.sqrt(3.0)-1.0);var s=(xin+yin)*F2;var i=Math.floor(xin+s);var j=Math.floor(yin+s);var G2=(3.0-Math.sqrt(3.0))/6.0;var t=(i+j)*G2;var X0=i-t;var Y0=j-t;var x0=xin-X0;var y0=yin-Y0;var i1,j1;if(x0>y0){i1=1;j1=0}else{i1=0;j1=1}var x1=x0-i1+G2;var y1=y0-j1+G2;var x2=x0-1.0+2.0*G2;var y2=y0-1.0+2.0*G2;var ii=i&255;var jj=j&255;var gi0=this.perm[ii+this.perm[jj]]%12;var gi1=this.perm[ii+i1+this.perm[jj+j1]]%12;var gi2=this.perm[ii+1+this.perm[jj+1]]%12;var t0=0.5-x0*x0-y0*y0;if(t0<0){n0=0.0}else{t0*=t0;n0=t0*t0*this.dot(this.grad3[gi0],x0,y0)}var t1=0.5-x1*x1-y1*y1;if(t1<0){n1=0.0}else{t1*=t1;n1=t1*t1*this.dot(this.grad3[gi1],x1,y1)}var t2=0.5-x2*x2-y2*y2;if(t2<0){n2=0.0}else{t2*=t2;n2=t2*t2*this.dot(this.grad3[gi2],x2,y2)}return 70.0*(n0+n1+n2)};SimplexNoise.prototype.noise3d=function(xin,yin,zin){var n0,n1,n2,n3;var F3=1.0/3.0;var s=(xin+yin+zin)*F3;var i=Math.floor(xin+s);var j=Math.floor(yin+s);var k=Math.floor(zin+s);var G3=1.0/6.0;var t=(i+j+k)*G3;var X0=i-t;var Y0=j-t;var Z0=k-t;var x0=xin-X0;var y0=yin-Y0;var z0=zin-Z0;var i1,j1,k1;var i2,j2,k2;if(x0>=y0){if(y0>=z0){i1=1;j1=0;k1=0;i2=1;j2=1;k2=0}else if(x0>=z0){i1=1;j1=0;k1=0;i2=1;j2=0;k2=1}else{i1=0;j1=0;k1=1;i2=1;j2=0;k2=1}}else{if(y0<z0){i1=0;j1=0;k1=1;i2=0;j2=1;k2=1}else if(x0<z0){i1=0;j1=1;k1=0;i2=0;j2=1;k2=1}else{i1=0;j1=1;k1=0;i2=1;j2=1;k2=0}}var x1=x0-i1+G3;var y1=y0-j1+G3;var z1=z0-k1+G3;var x2=x0-i2+2.0*G3;var y2=y0-j2+2.0*G3;var z2=z0-k2+2.0*G3;var x3=x0-1.0+3.0*G3;var y3=y0-1.0+3.0*G3;var z3=z0-1.0+3.0*G3;var ii=i&255;var jj=j&255;var kk=k&255;var gi0=this.perm[ii+this.perm[jj+this.perm[kk]]]%12;var gi1=this.perm[ii+i1+this.perm[jj+j1+this.perm[kk+k1]]]%12;var gi2=this.perm[ii+i2+this.perm[jj+j2+this.perm[kk+k2]]]%12;var gi3=this.perm[ii+1+this.perm[jj+1+this.perm[kk+1]]]%12;var t0=0.6-x0*x0-y0*y0-z0*z0;if(t0<0){n0=0.0}else{t0*=t0;n0=t0*t0*this.dot(this.grad3[gi0],x0,y0,z0)}var t1=0.6-x1*x1-y1*y1-z1*z1;if(t1<0){n1=0.0}else{t1*=t1;n1=t1*t1*this.dot(this.grad3[gi1],x1,y1,z1)}var t2=0.6-x2*x2-y2*y2-z2*z2;if(t2<0){n2=0.0}else{t2*=t2;n2=t2*t2*this.dot(this.grad3[gi2],x2,y2,z2)}var t3=0.6-x3*x3-y3*y3-z3*z3;if(t3<0){n3=0.0}else{t3*=t3;n3=t3*t3*this.dot(this.grad3[gi3],x3,y3,z3)}return 32.0*(n0+n1+n2+n3)};
var noises = {};
function rnd(s, a, x, y, z){
    if(!noises[s]) noises[s] = new SimplexNoise({random:new Alea(s)});
    return (noises[s].noise((x||0)*a,(y||0)*a,(z||0)*a) * 0.5) + 0.5
}
function getCursorPosition(canvas, event) {
    var rect = canvas.getBoundingClientRect();
    var x = event.clientX - rect.left;
    var y = event.clientY - rect.top;
    return {x:x,y:y};
}

var canvas, ctx, width, height;
var skins = {
    'blox:moblox': {
        left: [
            document.getElementById('skin/moblox/1'),
            document.getElementById('skin/moblox/2'),
            document.getElementById('skin/moblox/3'),
            document.getElementById('skin/moblox/4')
        ],
        right: [
            document.getElementById('skin/moblox/1:flipped'),
            document.getElementById('skin/moblox/2:flipped'),
            document.getElementById('skin/moblox/3:flipped'),
            document.getElementById('skin/moblox/4:flipped')
        ],
        height: 28,
        width: 14
    },'blox:f1': {
        left: [
            document.getElementById('skin/f1/1'),
            document.getElementById('skin/f1/2'),
            document.getElementById('skin/f1/3'),
            document.getElementById('skin/f1/4')
        ],
        right: [
            document.getElementById('skin/f1/1:flipped'),
            document.getElementById('skin/f1/2:flipped'),
            document.getElementById('skin/f1/3:flipped'),
            document.getElementById('skin/f1/4:flipped')
        ],
        height: 29,
        width: 14
    },'blox:f2': {
        left: [
            document.getElementById('skin/f2/1'),
            document.getElementById('skin/f2/2'),
            document.getElementById('skin/f2/3'),
            document.getElementById('skin/f2/4')
        ],
        right: [
            document.getElementById('skin/f2/1:flipped'),
            document.getElementById('skin/f2/2:flipped'),
            document.getElementById('skin/f2/3:flipped'),
            document.getElementById('skin/f2/4:flipped')
        ],
        height: 29,
        width: 14
    },'blox:m1': {
        left: [
            document.getElementById('skin/m1/1'),
            document.getElementById('skin/m1/2'),
            document.getElementById('skin/m1/3'),
            document.getElementById('skin/m1/4')
        ],
        right: [
            document.getElementById('skin/m1/1:flipped'),
            document.getElementById('skin/m1/2:flipped'),
            document.getElementById('skin/m1/3:flipped'),
            document.getElementById('skin/m1/4:flipped')
        ],
        height: 30,
        width: 14
    },
    'blox:m3': {
        left: [
            document.getElementById('skin/m3/1'),
            document.getElementById('skin/m3/2'),
            document.getElementById('skin/m3/3'),
            document.getElementById('skin/m3/4')
        ],
        right: [
            document.getElementById('skin/m3/1:flipped'),
            document.getElementById('skin/m3/2:flipped'),
            document.getElementById('skin/m3/3:flipped'),
            document.getElementById('skin/m3/4:flipped')
        ],
        height: 28,
        width: 14
    },
}

var currentframe = 0;
var fps = 0;
var lastZeroFrameTime = Date.now();

var zoom = 2;
var camera = {x:0,y:0};
var debug = false;

var pointers = {};

var chat = {
    open: false,
    message: "",
    messages: []
};

var menu = "MainMenu";
var connection = null;
var blockCount = 0;
var w = null;
var id = 0;

function receive(msg){
    switch(msg.message){
        case "textures":
            menu = "Loading";
            blockCount = 0;
            for(var i = 0;i < msg.textures.length;i++){
                var image = new Image();
                image.onload = function () {
                    blockCount++;
                    if (blockCount == msg.textures.length) {
                        menu = null;
                    }
                };
                image.src = `${connection?`http${connection.startsWith('localhost')?'':'s'}://`+connection:''}/resources/images/blocks/${msg.textures[i]}.png`;
                blocks[msg.textures[i]] = image;
            }
            break;
        case "world":
            if(msg.id === false) return alert('Someone is already logged in with that username.');
            w = msg.world;
            id = msg.id;
            break;
        case "region":
            w.regions[msg.region] = msg.data;
            break;
        case "players":
            w.players = msg.players
            break;
        case "command_response":
            chat.messages.unshift([msg.text,"gray",Date.now()]);
            break;
        case "chat":
            chat.messages.unshift([msg.player.name+"> "+msg.text,"white",Date.now()]);
            break;
        case "death":
            chat.messages.unshift([msg.player.name+" "+msg.deathMessage,"red",Date.now()]);
            break;
        case "leave":
            chat.messages.unshift([msg.player.name+" left the game","yellow",Date.now()]);
            break;
        case "join":
            chat.messages.unshift([msg.player.name+" joined the game","limegreen",Date.now()]);
            break;
        default:
            break;
    }
}
function start(name){
    send({message: "connect",username:name});
}
function exit(){
    stop();
    menu = "MainMenu";
    w = null;
    chat = {
        message:"",
        messages:[],
        open:false
    }
    send = function(){};
    stop = function(){};
}

canvas = document.getElementById('canvas');
ctx = canvas.getContext('2d');
ctx.imageSmoothingEnabled = false;
ctx.mozImageSmoothingEnabled = false;
ctx.webkitImageSmoothingEnabled = false;
window.addEventListener('resize', function(event){
    resizeCanvasToDisplaySize(canvas);
});
resizeCanvasToDisplaySize(canvas);

function onPointerDown(device){
    if(menu == "MainMenu"){

    } else if(w){
        let x = Math.floor(pointers[device].startx/zoom/16 + camera.x);
        let y = Math.ceil(camera.y + (height - pointers[device].starty)/zoom/16);
        console.log(x,y);
        send({
            message: "setBlock",
            x: x,
            y: y,
            block: w.blocks[(blockAt(x,y)+1)%w.blocks.length]
        });
    }
}
function onPointerMove(device){

}
function onPointerUp(device){
    if(menu == "MainMenu"){
        let lg = {
            left: ((width - Math.min(592,width)) / 2) + 10,
            top: 10,
            width: Math.min(592,width) - 20, 
            height: Math.min(176,width/(592/176)) - 20,
            pixel: (Math.min(592,width) - 20) / 592
        }
        let sp = {
            left: ((width - Math.min(1032,width)) / 2) + 10,
            top: lg.top + lg.height + 30,
            width: Math.min(1032,width) - 20, 
            height: Math.min(144,width/(1032/144)) - 20,
            pixel: (Math.min(1032,width) - 20) / 1032
        }
        let mp = {
            left: ((width - Math.min(1032,width)) / 2) + 10,
            top: sp.top + sp.height + 30,
            width: Math.min(1032,width) - 20, 
            height: Math.min(144,width/(1032/144)) - 20,
            pixel: (Math.min(1032,width) - 20) / 1032
        }
        if(pointers[device].starty > sp.top && pointers[device].starty < sp.top+sp.height){
            connect("client","Player");
        } else if(pointers[device].starty > mp.top && pointers[device].starty < mp.top+mp.height){
            var server = prompt("Server IP:");
            var name = prompt("Username:");
            connect(server,name);
        }
    } else {

    }
}

document.addEventListener("contextmenu", function(e){
    e.preventDefault();
}, false);
canvas.addEventListener("mousedown", function(event){
    pointers["mouse"] = {button: event.button, down: true, startTime: Date.now(), startx: getCursorPosition(canvas, event).x, starty: getCursorPosition(canvas, event).y}
    onPointerDown("mouse");
});
canvas.addEventListener("mousemove", function(event){
    if(!pointers["mouse"])pointers["mouse"] = {down: false, startTime: null, startx: null, starty: null};
    pointers["mouse"].x = getCursorPosition(canvas, event).x;
    pointers["mouse"].y = getCursorPosition(canvas, event).y;
    console.log('mmove')
    onPointerMove("mouse");
});
canvas.addEventListener("mouseup", function(event){
    pointers["mouse"].down = false;
    onPointerUp("mouse");
});

document.addEventListener("keydown", function(event) {
    keysdown[event.key] = true;
    if(chat.open) {
        if(event.key == "Escape"){
            chat.message = "";
            chat.open = false;
            return;
        } else if(event.key == "Backspace"){
            chat.message = chat.message.slice(0,-1);
        } else if(event.key == "Enter"){
            send({message:"chat",text:chat.message});
            chat.message = "";
            chat.open = false;
        } else if(event.key.length == 1){
            if(keysdown["Shift"]){
                chat.message+=event.key.toUpperCase();
            } else {
                chat.message+=event.key
            }
        }
    } else if(event.key == "/" || event.key == "t"){
        chat.open = true;
        if(event.key == "/"){
            chat.message = "/";
        }
    } else if(event.key == "`"){
        debug = !debug;
    } else if(event.key == "p"){
        send({message:"nextSkin"});
    } else if(event.key == "Escape"){
        exit();
    } else if("12345678".split('').includes(event.key)){
        send({message:"switchSlot",slot:Number(event.key)-1});
    } else if(event.key == "="){
        if(zoom<9.9){
            zoom+=0.1;
        }
    } else if(event.key == "-"){
        if(zoom>0.6){
            zoom-=0.1;
        }
    }
    if(!chat.open){
        send({
            message:"keys",
            keys:[
                keysdown.a,
                keysdown.d,
                keysdown[" "]
            ]
        });
    }
    event.preventDefault()
    return false;
})
document.addEventListener("keyup", function(event) {
    keysdown[event.key] = false;
    if(!chat.open){
        send({
            message:"keys",
            keys:[
                keysdown.a,
                keysdown.d,
                keysdown[" "]
            ]
        });
    }
    return false;
});
var keysdown = {};

var blocks = {};
function block(id,x,y){
    if(!w.textures[w.blocks[id]]) {
        console.log('Missing block', id);
        // if(document.getElementById(`block/${w.blocks[id]}`)){
        //     blocks[id] = document.getElementById(`block/${w.blocks[id]}`);
        // } else {
        //     blocks[id] = [];
        //     for(var i = 1;document.getElementById(`block/${w.blocks[id]}/${i}`);i++){
        //         blocks[id].push(document.getElementById(`block/${w.blocks[id]}/${i}`));
        //     }
        // }
        return blocks['blox:missingno'];
    }
    return blocks[w.textures[w.blocks[id]][ Math.floor( rnd( parseInt(w.seeds.slice(30,32),2), 10, x, y ) * w.textures[w.blocks[id]].length) ]];
}
var images = {};
function image(id){
    if(!images[id]) images[id] = document.getElementById(`image/${id}`);
    return images[id];
}
var itemImgs = {};
function itemImg(id){
    if(!itemImgs[id]) itemImgs[id] = document.getElementById(`item/${id}`);
    return itemImgs[id];
}

function blockAt(i,j){
    try {
        return w.regions[`${Math.floor(i/16)}-${Math.floor(j/16)}`][Math.abs(Math.floor(i))%16][Math.abs(Math.floor(j))%16]
    } catch(e){
        return w.blocks.indexOf('blox:barrier');
    }
}

function frame(){
    ctx.imageSmoothingEnabled = false;
    ctx.mozImageSmoothingEnabled = false;
    ctx.webkitImageSmoothingEnabled = false;
    requestAnimationFrame(frame);
    currentframe = (currentframe+1)%60;
    if(currentframe == 0){
        fps = 60 / ((Date.now() - lastZeroFrameTime) / 1000);
        lastZeroFrameTime = Date.now();
        console.log("Client FPS:", fps);
    }
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    if(menu == "MainMenu"){
        // ctx.font = "20px Georgia";
        // ctx.fillText("Hello World!", 10, 50);
        ctx.fillStyle = "black";
        // fitText({},"BloxR",20,20,width-40,100);

        let lg = {
            left: ((width - Math.min(592,width)) / 2) + 10,
            top: 10,
            width: Math.min(592,width) - 20, 
            height: Math.min(176,width/(592/176)) - 20,
            pixel: (Math.min(592,width) - 20) / 592
        }
        ctx.drawImage(
            image('logo'), 
            lg.left, 
            lg.top,
            lg.width, 
            lg.height
        );
        let sp = {
            left: ((width - Math.min(1032,width)) / 2) + 10,
            top: lg.top + lg.height + 30,
            width: Math.min(1032,width) - 20, 
            height: Math.min(144,width/(1032/144)) - 20,
            pixel: (Math.min(1032,width) - 20) / 1032
        }
        ctx.drawImage(
            image('singleplayer'), 
            sp.left, 
            sp.top,
            sp.width, 
            sp.height
        );
        let mp = {
            left: ((width - Math.min(1032,width)) / 2) + 10,
            top: sp.top + sp.height + 30,
            width: Math.min(1032,width) - 20, 
            height: Math.min(144,width/(1032/144)) - 20,
            pixel: (Math.min(1032,width) - 20) / 1032
        }
        ctx.drawImage(
            image('multiplayer'), 
            mp.left, 
            mp.top,
            mp.width, 
            mp.height
        );
        
        // fitText({},"Singleplayer",20,ib.top+ib.height+30,width-40,100);
        // fitText({},"Multiplayer",20,ib.top+ib.height+150,width-40,100);
    } else if(menu == "Loading") {
        fitText({},"Loading...",20,20,width-40,100);
    } else if (menu == "Pause") {

    } else if(w.height) {
        camera = {
            x: (w.players.find(i=>i.id==id).x - (width/32/zoom)),
            y: (w.players.find(i=>i.id==id).y - (height/32/zoom))
        }
        renderTiles(w);
        for(var i = 0;i < w.players.length;i++){
            if(w.players[i].online && w.players[i].id != id)renderPlayer(w.players[i]);
        }
        renderPlayer(w.players.find(i=>i.id==id),true);
        renderEntities(w.entities);
        renderHUD(w.players.find(i=>i.id==id));
        renderChat(chat);
    }
}
// function renderParticles(){
//     for(var i = 0;i < w.particles.length;i++){

//     }
// }
function renderTiles(){
    for(var i = camera.x-1;i < camera.x+(width/16)/zoom+1;i++){
        for(var j = camera.y-1;j < camera.y+(height/16)/zoom+2;j++){
            let r = blockAt(i,j);
            if(camera.x < 0) i-=1;
            if(camera.y < 0) j-=1;
            try{
                ctx.drawImage(
                    block(r,Math.floor(i),Math.floor(j)), 
                    ( i - camera.x - ( camera.x % 1 ) ) * 16 * zoom, 
                    height - (( j - camera.y - ( camera.y % 1 ) ) * 16 * zoom),
                    16*zoom, 
                    16*zoom
                );
                if(debug)ctx.strokeRect(
                    ( i - camera.x - ( camera.x % 1 ) ) * 16 * zoom, 
                    height - (( j - camera.y - ( camera.y % 1 ) ) * 16 * zoom),
                    16*zoom, 
                    16*zoom
                )
            } catch(e){
                console.log(r);
            }
            if(camera.x < 0) i+=1;
            if(camera.y < 0) j+=1;
        }
    }
}
function renderPlayer(player,nonametag){
    if(!player.state) console.log(player);
    let skin = skins[player.state||'blox:moblox'][player.facing||"left"][player.moving?Math.floor(currentframe/60*skins[player.state||'blox:moblox'][player.facing||"left"].length):0];
    ctx.drawImage(
        skin,
        ((player.x * 16) - (skin.width/2) - (camera.x*16))*zoom,
        height-((player.y * 16) - 16 + skin.height - (camera.y*16))*zoom, 
        skin.width*zoom, 
        skin.height*zoom
    )
    if(debug)ctx.strokeRect(
        ((player.x * 16) - (skin.width/2) - (camera.x*16))*zoom,
        height-((player.y * 16) - 16 + skin.height - (camera.y*16))*zoom, 
        // width/2 - (player.width/2)*zoom,
        // height/2 - ((player.height/2))*zoom, 
        skin.width*zoom, 
        skin.height*zoom
    );
    for(var i = 0;i < player.hitboxes.length;i++){
        var path=new Path2D();
        let h = player.hitboxes[i].t;
        let x = ((player.x * 16) - (camera.x*16))*zoom;
        let y = height-((player.y * 16) - (camera.y*16))*zoom;
        path.moveTo(x+(h.a.x,y*zoom),y+(h.a.y*zoom));
        path.lineTo(x+(h.b.x,y*zoom),y+(h.b.y*zoom));
        path.lineTo(x+(h.c.x,y*zoom),y+(h.c.y*zoom));
        ctx.fillStyle="red";
        ctx.fill(path);
    }
    if(!nonametag) {
        ctx.font = "10px 'Press Start 2P'";
        ctx.fillText(
            player.name, 
            ((player.x * 16) - (camera.x*16))*zoom - (ctx.measureText(player.name).width/2),
            height-((player.y * 16) + (skin.height/2) - (camera.y*16))*zoom - 5
        );
        if(debug)ctx.strokeRect(
            ((player.x * 16) - (camera.x*16))*zoom - (ctx.measureText(player.name).width/2),
            height-((player.y * 16) + (skin.height/2) - (camera.y*16))*zoom - 5 - 10,
            // width/2 - (player.width/2)*zoom,
            // height/2 - ((player.height/2))*zoom, 
            ctx.measureText(player.name).width, 
            10
        );
    }
}
function renderEntities(){

}
function renderChat(chat){
    let heightOffset = Math.min(55,width/(400/55)) + 20;
    if(chat.open){
        ctx.fillStyle="rgba(0,0,0,0.8)";
        ctx.fillRect(10,50,width-20,height-heightOffset-40)
        ctx.fillStyle="white";
        for(var i = 0;i < chat.messages.slice(-((height-150)/14)).length;i++){
            ctx.fillStyle=chat.messages[i][1];
            fitText({align:"left",maxSize:10},chat.messages[i][0],15,height-heightOffset-50-(i*14),width-30,10,null)
        }
        ctx.fillStyle="white";
        fitText({align:"left"},chat.message,15,height-heightOffset-20,width-30,20,null)
    } else if(chat.messages.length>0) {
        ctx.fillStyle="rgba(0,0,0,0.7)";
        if(chat.messages.filter(i=>i[2]>Date.now()-3000).length>0) ctx.fillRect(10,height-heightOffset-20-(chat.messages.filter(i=>i[2]>Date.now()-3000).length*14),width-20,chat.messages.filter(i=>i[2]>Date.now()-3000).length*14+4)
        ctx.fillStyle="white";
        for(var i = 0;i < chat.messages.filter(i=>i[2]>Date.now()-3000).length;i++){
            ctx.fillStyle=chat.messages.filter(i=>i[2]>Date.now()-3000)[i][1];
            fitText({align:"left",maxSize:10},chat.messages.filter(i=>i[2]>Date.now()-3000)[i][0],15,height-heightOffset-20-(3*13)-(i*14),width-30,40,null)
        }
    }
}
function renderHUD(p){
    let x = 10;
    let y = 10;
    let flashing = false;
    if(p.health < 2) flashing = true;
    for(var i = 0;i < 10;i++){
        while(x >= (canvas.width/2-25)){
            x = 10;
            y += 25;
        }
        drawHeart(x-(flashing?Math.random()/2:0),y-(flashing?Math.random()/2:0),0.2-(Math.random()/(flashing?90:200)),p.health-i);
        x+=25;
    }
    flashing = false;
    x = 10;
    y = 10;
    if(p.air < 2000) flashing = true;
    for(var i = 0;i < 10;i++){
        while(x >= (canvas.width/2-25)){
            x = 10;
            y += 25;
        }
        if(p.air<10000)drawAir((width-x-18)-(flashing?Math.random()/2:0),y-(flashing?Math.random()/2:0),0.2-(flashing?Math.random()/90:0),(p.air/1000)-i);
        x+=25;
    }
    let ib = {
        left: (width - Math.min(400,width)) / 2,
        top: height - Math.min(55,width/(400/55)) - 10,
        width: Math.min(400,width), 
        height: Math.min(55,width/(400/55)),
        pixel: Math.min(400,width) / 400
    }
    ctx.drawImage(image("invbar"), ib.left, ib.top, ib.width, ib.height);
    for(var i = 0;i < 8;i++){
        let item = p.inventory.items[i];
        if(p.selectedSlot==i){
            ctx.drawImage(image("invbarSelector"),ib.left+((i*49)*ib.pixel),ib.top,58*ib.pixel,55*ib.pixel);
        }
        if(item.item){
            ctx.drawImage(itemImg(item.item),ib.left+((10.5+i*49)*ib.pixel),ib.top+(9*ib.pixel),37*ib.pixel,37*ib.pixel)
        }
    }
}

function drawAir(x,y,s,fill){
    if(fill > 0){
        let im = image("airbubble");
        ctx.drawImage(im, x, y, 100*s, 100*s);
        if(debug)ctx.strokeRect(x,y,100*s,100*s);
    }
}

function drawHeart(x,y,s,fill){
    let im = image("emptyheart");
    if(fill >= 0.5){
        im = image("halfheart");
    }
    if(fill >= 1){
        im = image("heart");
    }
    ctx.drawImage(im, x, y, 100*s, 100*s);
    if(debug)ctx.strokeRect(x,y,100*s,100*s);
}
function fitText(o,t,x,y,w,h,font){
    var m;
    for(var i = o.maxSize||h;i > 0;i-=1){
        ctx.font = `${i}px ${font||"'Press Start 2P'"}`;
        m = ctx.measureText(t);
        if(m.width <= w){
            break;
        }
    }
    let left = x+(w/2) - (m.width/2);
    if(o.align=="left") left = x;
    if(o.align=="right") left = x+(w/2)-(m.width);
    ctx.fillText(
        t,
        left,
        y+h
    );
    return {s: i,x: left,y: y+h};
}

var requestAnimationFrame = window.requestAnimationFrame
    || window.webkitRequestAnimationFrame
    || window.mozRequestAnimationFrame
    || window.msRequestAnimationFrame
    || function(callback) { return setTimeout(callback, 1000 / 60); };

requestAnimationFrame(frame);

var send = function(){};
var stop = function(){};

function connect(ip,name){
    if(ip == "client"){
        let d = runClientServer();
        connection = null;
        d.recFunc(receive);
        send = d.send;
        stop = d.stop;
        start(name);
    } else {
        var serverConnectionSocket = new WebSocket(`ws${ip.startsWith('localhost')?'':'s'}://${ip}`);
        serverConnectionSocket.onopen = function (event) {
            connection = ip;
            send = function send(data){serverConnectionSocket.send(JSON.stringify(data))};
            stop = function(){serverConnectionSocket.close();}
            start(name);
        };
        serverConnectionSocket.onmessage = function (message) {
            receive(JSON.parse(message.data));
        }
        serverConnectionSocket.onclose = function (){
            exit();
        }
    }
}